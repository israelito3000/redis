# Redis-Proxy #

This is a software that works as a tunnel between your application and your Redis server, this application connect every n seconds to the sentinels and ask for the master server, then it will tunnel all your request to the master only, if the master is down and the sentinels perform the failover, the next master will be detected by this application and redirect all calls to that server.

![redis-proxy.jpg](https://bitbucket.org/repo/KA5jo7/images/2781822918-redis-proxy.jpg)

### How do I get set up? ###

* Redis proxy can run as a windows service or as a console application:

![redis-proxy-console.jpg](https://bitbucket.org/repo/KA5jo7/images/2331034236-redis-proxy-console.jpg)

* To run as console: -console
* To install as a service: -install or -i
* To uninstall the service: -uninstall or -u

### Configuration parameters ###


```
#!xml

<configuration>
  <appSettings>
    <add key="sentinels" value="172.21.176.51:8099,172.21.176.52:8099"/>  
    <add key="check_sentinels_seconds" value="3"/>
    
    <add key="local" value="127.0.0.1:8097"/>
    <add key="keepalive" value="true"/>
    <add key="timeout" value="0"/>
  </appSettings>
</configuration>
```

#### The configuration parameters presented below are: ####

  - sentinels: you have to list all the sentinels connections separated by comma.

  - check_sentinels_seconds: number of seconds between the sentinel checking for master connection.

  - local: the local IP address where the proxy is going to listen waiting for connections.

  - keepalive: enable the keepalive feature of the socket connected to master redis server.

  - timeout: Represent the time out for socket connection, after this time the socket will be disconnected automatically from the master redis. (0 = disabled).


### How to connect to the proxy ###

The proxy simply act as a Redis Server, just set the IP and port to connect to it, using the client you want to connect to it.

### Download binaries for Windows ###

[RedisProxy-x86.zip](https://bitbucket.org/israelito3000/redis/downloads/RedisProxy-x86.zip)

[RedisProxy-x64.zip](https://bitbucket.org/israelito3000/redis/downloads/RedisProxy-x64.zip)

### Contribution guidelines ###

* Code review

### Who do I talk to? ###

* Repo owner or admin

### Do you found useful this tool? Invite me a coffee ###

[![btn_donateCC_LG.gif](https://bitbucket.org/repo/KA5jo7/images/876482288-btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=D5WMZEPTX4KY6&lc=MX&item_name=Israel%20Coffee%20for%20Brain&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)