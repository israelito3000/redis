﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace RedisProxy
{
    class Program
    {
        static RunAsService s = new RunAsService();
        
        static int Main(string[] args)
        {
            bool install = false, uninstall = false, console = false, rethrow = false;
            try
            {
                foreach (string arg in args)
                {
                    switch (arg)
                    {
                        case "-i":
                        case "-install":
                            install = true; break;
                        case "-u":
                        case "-uninstall":
                            uninstall = true; break;
                        case "-c":
                        case "-console":
                            console = true; break;
                        default:
                            Global.Log("Argument not expected: " + arg);
                            break;
                    }
                }

                if (uninstall)
                {
                    ProjectInstaller.Install(true, args);
                }
                if (install)
                {
                    ProjectInstaller.Install(false, args);
                }

                if (console)
                {
                    Global.Log("Starting...");
                    StartUp();
                    Global.Log("System running; press any key to stop");
                    Console.ReadKey(true);
                    ShutDown();
                    Global.Log("System stopped");
                }
                else if (!(install || uninstall))
                {
                    rethrow = true; // so that windows sees error...

                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] { new RunAsService() };
                    ServiceBase.Run(ServicesToRun);
                    rethrow = false;
                }
                return 0;
            }
            catch (Exception ex)
            {
                if (rethrow) throw;
                Global.Log(ex.Message);
                return -1;
            }
        }

        static void StartUp()
        {
            s.StartUp();
        }

        static void ShutDown()
        {
            s.Shutdown();
        }

    }
}
