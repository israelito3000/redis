﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedisProxy
{
    public static class Global
    {
        public static string logFile = "proxy.log";
        public static string masterIP = "";
        public static string listenIP = "";
        public static int firstHash = 0;

        public static void Log(string txt){
            string folder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string line = DateTime.Now.ToString("yyy-MM-dd HH:mm:ss") + " - " + txt ;
           // TryerHelper.Try(delegate { File.AppendAllText(folder + @"\" + logFile, line + "\r\n"); }).Retry(5).IgnoreException(typeof(Exception)).Execute();
            Console.WriteLine(line);
            //Thread.Sleep(300);
        }
    }

    public static class Repeat
    {
        public static Task Interval(
            TimeSpan pollInterval,
            Action action,
            CancellationToken token)
        {
            // We don't use Observable.Interval:
            // If we block, the values start bunching up behind each other.
            return Task.Factory.StartNew(
                () =>
                {
                    for (; ; )
                    {
                        if (token.WaitCancellationRequested(pollInterval))
                            break;

                        action();
                    }
                }, token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }
    }

    public static class CancellationTokenExtensions
    {
        public static bool WaitCancellationRequested(
            this CancellationToken token,
            TimeSpan timeout)
        {
            return token.WaitHandle.WaitOne(timeout);
        }
    }

    public class FordwardAddress
    {
        public int Port { get; set; }
        public string Ip { get; set; }
    }
}
