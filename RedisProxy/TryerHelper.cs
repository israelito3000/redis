﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RedisProxy
{
    
    public class TryerHelper
    {
        public class TryerException : ApplicationException
        {
            public TryerException() : base() { }
            public TryerException(string message) : base(message) { }
        }

        public class TryerQuery
        {

            private object _method;
            private List<Type> _iexceptions = new List<Type>();
            private List<string> _iexceptiontext = new List<string>();
            private int _retry = 0;
            private int _retryinterval = 100;

            public TryerQuery(object method)
            {
                _method = method;
            }

            public TryerQuery RetryInterval(int intervalmilliseconds)
            {
                _retryinterval = intervalmilliseconds;
                return this;
            }

            public TryerQuery Retry(int times)
            {
                _retry = times;
                return this;
            }

            public TryerQuery IgnoreException(Type exceptionType)
            {
                _iexceptions.Add(exceptionType);
                return this;
            }

            public TryerQuery IgnoreExceptionMessage(string message)
            {
                _iexceptiontext.Add(message);
                return this;
            }

            private void Action(params object[] args)
            {
                ((Action)_method).DynamicInvoke(args);
            }

            private T Func<T>(params object[] args)
            {
                T res = (T)((Func<T>)_method).DynamicInvoke(args);
                return res;
            }

            public T Execute<T>(params object[] args)
            {
                int retries = _retry;
                do
                {
                    try
                    {
                        if (_method is Func<T>)
                            return Func<T>(args);
                    }
                    catch (Exception ex)
                    {
                        bool continueex = false;
                        foreach (var e in _iexceptions)
                        {
                            if (e == ex.InnerException.GetType()) { continueex = true; break; }
                        }
                        if (!continueex)
                            foreach (var e in _iexceptiontext)
                            {
                                if (e == ex.InnerException.Message) { continueex = true; break; }
                            }
                        if (continueex)
                        {
                            retries--;
                            Thread.Sleep(_retryinterval);
                            continue;
                        }
                        throw ex;
                    }
                } while (retries >= 0);
                throw new TryerException("Max attempt reached: " + _retry);
            }

            public void Execute(params object[] args)
            {
                int retries = _retry;
                do
                {
                    try
                    {
                        if (_method is Action)
                        {
                            Action(args);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        bool continueex = false;
                        foreach (var e in _iexceptions)
                        {
                            if (e == ex.InnerException.GetType()) { continueex = true; break; }
                        }
                        if (!continueex)
                            foreach (var e in _iexceptiontext)
                            {
                                if (e == ex.InnerException.Message) { continueex = true; break; }
                            }
                        if (continueex)
                        {
                            retries--;
                            Thread.Sleep(_retryinterval);
                            continue;
                        }
                        throw ex;
                    }
                } while (retries >= 0);
                throw new TryerException("Max attempt reached: " + _retry);
            }
        }

        public static TryerQuery Try(Action method)
        {
            return new TryerQuery(method);
        }
        public static TryerQuery Try<T>(Func<T> method)
        {
            return new TryerQuery(method);
        }
    }
}


