﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace RedisProxy
{
    public class RunAsService : ServiceBase
    {
        TcpForwarderSlim tunnel = null;
        bool keepalive = false;
        int timeout = 0;
        int buffersize = 8192;

        public RunAsService()
        {
            this.ServiceName = "Redis-Proxy";
        }

        public void StartUp()
        {

            keepalive = bool.Parse(ConfigurationManager.AppSettings["keepalive"]);
            timeout = int.Parse(ConfigurationManager.AppSettings["timeout"]);
            buffersize = int.Parse(ConfigurationManager.AppSettings["buffersize"]);

            string[] sentinels = ConfigurationManager.AppSettings["sentinels"].Split(',');
            Global.listenIP = ConfigurationManager.AppSettings["local"];
            string local_ip = ConfigurationManager.AppSettings["local"].Split(':')[0];
            int local_port = int.Parse(ConfigurationManager.AppSettings["local"].Split(':')[1]);
            int check_sentinels_seconds = int.Parse(ConfigurationManager.AppSettings["check_sentinels_seconds"]);
            tunnel = new TcpForwarderSlim();
            
            RedisSentinelChecker.Masterchange += RedisSentinelChecker_Masterchange;
            RedisSentinelChecker.SentinelCheck += RedisSentinelChecker_SentinelCheck;
            RedisSentinelChecker.StartCheckingSentinels(sentinels, "mymaster", check_sentinels_seconds);
                                    
            ThreadStart tsd = new ThreadStart(delegate
            {                
                tunnel.Start(new IPEndPoint(IPAddress.Any, local_port), keepalive, timeout, buffersize);
            });
            new Thread(tsd).Start();


        }

        private void RedisSentinelChecker_SentinelCheck(object sender, EventArgs e)
        {
            List<TcpForwarderSlim> toDelete = new List<TcpForwarderSlim>();
            lock (SocketsRepository.objlock)
            {
                foreach (var socket in SocketsRepository.Sockets)
                {
                    if (!socket.SourceSocket.Connected)
                    {
                        toDelete.Add(socket);
                    }
                }

                foreach (var socket in toDelete)
                {
                    if ((socket.MainSocket != null) && (socket.MainSocket.Connected)) 
                        socket.MainSocket.Close();
                    socket.buffer = null;
                    SocketsRepository.Sockets.Remove(socket);                    
                }
            }
            if (toDelete.Count > 0) Global.Log("Removing " + toDelete.Count + " socket.");
        }

        private void RedisSentinelChecker_Masterchange(object sender, EventArgs e)
        {
            Global.Log("Analyzing Sockets Repository: " + SocketsRepository.Sockets.Count + " sockets.");
            List<TcpForwarderSlim> toDelete = new List<TcpForwarderSlim>();
            lock (SocketsRepository.objlock)
            {
                foreach (var socket in SocketsRepository.Sockets)
                {

                    if ((socket.MainSocket != null) && (socket.MainSocket.Connected)) socket.MainSocket.Disconnect(false);
                    if ((socket.SourceSocket != null) && (socket.SourceSocket.Connected))
                    {
                        var remote = new IPEndPoint(IPAddress.Parse(Global.masterIP.Split(':')[0]), int.Parse(Global.masterIP.Split(':')[1]));
                        socket.Connect(remote);
                    }
                }
                if (SocketsRepository.Sockets.Count > 0) Global.Log("Redirected " + SocketsRepository.Sockets.Count + " socket.");
            }
        }

        public void Shutdown()
        {
            RedisSentinelChecker.StopCheckingSentinels();
            tunnel.Stop();
        }


        protected override void OnStart(string[] args)
        {
            StartUp();        
        }

        protected override void OnStop()
        {
            Shutdown();
        }
    }
}
