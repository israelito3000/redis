﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace RedisProxy
{
    public class SocketsRepository
    {
        public static List<TcpForwarderSlim> Sockets = new List<TcpForwarderSlim>();
        public static object objlock = new object();
    }

    public class SocketPair
    {
        public Socket SourceSocket { get; set; }
        public TcpForwarderSlim DestinationSocket { get; set; }
    }

    public class State : IDisposable
    {
        public TcpForwarderSlim Master { get; set; }
        public Socket SourceSocket { get; set; }
        public Socket DestinationSocket { get; set; }
        public byte[] Buffer { get; private set; }


        public State(Socket source, Socket destination, TcpForwarderSlim master, int buffersize)
        {
            SourceSocket = source;
            DestinationSocket = destination;
            Master = master;
            Buffer = new byte[buffersize];
        }

        public State(Socket source, Socket destination, TcpForwarderSlim master, int buffersize, byte[] buffer)
        {
            SourceSocket = source;
            DestinationSocket = destination;
            Master = master;
            Buffer = buffer;
        }

        public void Dispose()
        {
            Buffer = null;
        }
    }

    public class TcpForwarderSlim
    {
        public Socket MainSocket = null;
        public Socket SourceSocket = null;
        public bool keepalive;
        public int timeout;
        public string provcommand { get; set; }
        public string provcommandresponse { get; set; }
        public string lastcommand { get; set; }
        public string lastcommandresponse { get; set; }
        public int buffersize { get; set; }
        public byte[] buffer { get; set; }



        public TcpForwarderSlim()
        {
            provcommand = "";
            lastcommand = "";
            lastcommandresponse = "";
            provcommandresponse = "";
            buffersize = 8192;
        }

        public void Dispose()
        {
            buffer = null;
        }

        public void Start(IPEndPoint local, bool keepalive, int timeout, int buffersize)
        {
            Global.Log("Tunnel listening in: " + local);
            MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            MainSocket.Bind(local);
            MainSocket.Listen(10);
            while (true)
            {
                Socket source = null;
                try
                {
                    source = MainSocket.Accept();
                    Global.Log("Accepting connection.");
                }
                catch
                {
                    return;
                }
                var connection = new TcpForwarderSlim() { SourceSocket = source, keepalive = keepalive, timeout = timeout, buffersize = buffersize, buffer = new byte[buffersize] };
                lock (SocketsRepository.objlock)
                {
                    SocketsRepository.Sockets.Add(connection);
                }
                try
                {
                    TryerHelper.Try(delegate
                    {
                        if (string.IsNullOrEmpty(Global.masterIP)) throw new SocketException();
                        var remote = new IPEndPoint(IPAddress.Parse(Global.masterIP.Split(':')[0]), int.Parse(Global.masterIP.Split(':')[1]));
                        connection.Connect(remote); // Connect to Redis

                        var state = new State(source, connection.MainSocket, connection, buffersize, connection.buffer);
                        source.BeginReceive(state.Buffer, 0, state.Buffer.Length, 0, OnDataReceive, state); // Start receiving data from client
                    }).IgnoreException(typeof(SocketException)).Retry(3).RetryInterval(2000).Execute();
                }
                catch (Exception ex)
                {
                    Global.Log("Error: " + ex.ToString());
                    source.Close();
                    connection.MainSocket.Close();                    
                    continue;
                }
            }
        }

        public void Stop()
        {
            Global.Log("Stop Tunnel. ");
            MainSocket.Close();
        }

        // Connect to Redis
        public void Connect(EndPoint remoteEndpoint)
        {
            MainSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            MainSocket.Connect(remoteEndpoint);

            if (timeout > 0) MainSocket.ReceiveTimeout = timeout;
            MainSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, keepalive);

            var state = new State(MainSocket, SourceSocket, this, buffersize);
            MainSocket.BeginReceive(state.Buffer, 0, state.Buffer.Length, SocketFlags.None, OnDataReceive, state); // Start receiving data from Redis
        }


        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private void OnDataReceive(IAsyncResult result)
        {
            var state = (State)result.AsyncState;
            try
            {
                if (!state.SourceSocket.Connected) return;
                var bytesRead = state.SourceSocket.EndReceive(result);
                var curr = Encoding.ASCII.GetString(state.Buffer, 0, bytesRead);


                if (state.SourceSocket.LocalEndPoint.ToString().EndsWith("8097"))
                {

                    state.Master.provcommand += curr;
                    if (state.Master.provcommand.EndsWith("\r\n"))
                    {
                        state.Master.lastcommand = state.Master.provcommand;
                        state.Master.provcommand = "";
                    }
                }
                else
                {
                    state.Master.provcommandresponse += curr;
                    if (state.Master.provcommandresponse.EndsWith("\r\n"))
                    {
                        state.Master.lastcommandresponse = state.Master.provcommandresponse;
                        state.Master.provcommandresponse = "";
                    }
                }

                if (bytesRead > 0)
                {
                    Global.Log(" Receving data: " + bytesRead + " bytes from: " + state.SourceSocket.RemoteEndPoint);
                    bool sentdata = false;
                    int attempts = 3;
                    do
                    {
                        if (state.DestinationSocket.Connected)
                        {
                            Global.Log("Sending to: " + state.DestinationSocket.RemoteEndPoint);
                            state.DestinationSocket.Send(state.Buffer, bytesRead, SocketFlags.None);
                            sentdata = true;
                        }
                        else
                        if (state.Master.MainSocket.Connected)
                        {
                            Global.Log("Sending to: " + state.Master.MainSocket.RemoteEndPoint);
                            state.Master.MainSocket.Send(state.Buffer, bytesRead, SocketFlags.None);
                            sentdata = true;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(1000);
                            attempts--;
                            Global.Log("Socket is disconnected: " + state.DestinationSocket.RemoteEndPoint);
                        }
                    } while ((!sentdata) && (attempts > 0));

                    if (!sentdata)
                    {
                        if (state.SourceSocket.Connected)
                        {
                            //state.SourceSocket.Send(Encoding.ASCII.GetBytes("-ERR no connection.\r\n"));
                            state.SourceSocket.Close();
                            return;
                        }
                    }

                    state.SourceSocket.BeginReceive(state.Buffer, 0, state.Buffer.Length, 0, OnDataReceive, state);
                }
                else
                {

                    state.SourceSocket.Close();
                    state.DestinationSocket.Close();
                    state.Master.MainSocket.Close();                    
                    lock (SocketsRepository.objlock)
                    {
                        SocketsRepository.Sockets.Remove(state.Master);
                    }
                    state.Dispose();
                }
            }
            catch (SocketException ex)
            {
                if (!state.Master.MainSocket.Connected)
                {
                    if ((state.Master.lastcommand.ToLower() == "quit\r\n") && (state.Master.lastcommandresponse.ToLower() == "+ok\r\n"))
                    {

                        state.SourceSocket.Close();
                        state.DestinationSocket.Close();
                        lock (SocketsRepository.objlock)
                        {
                            SocketsRepository.Sockets.Remove(state.Master);
                        }
                        state.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Log("Error: " + ex.ToString());
                //state.SourceSocket.Close();
                //state.DestinationSocket.Close();                
            }
        }
    }
}
