﻿using CSRedis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedisProxy
{
    public class RedisSentinelChecker
    {
        private static int timeout = 1000;
        private static LinkedList<Tuple<string, int, RedisSentinelClient>> _sentinels;
        private static RedisConnectionPool _sentinelPool;
        private static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        public static event EventHandler Masterchange;
        public static event EventHandler SentinelCheck;

        public static void Add(string host, int port)
        {
            foreach (var sentinel in _sentinels)
            {
                if (sentinel.Item1 == host && sentinel.Item2 == port)
                    return;
            }
            //_sentinels.AddLast(Tuple.Create<string, int, RedisSentinelClient>(host, port, new RedisSentinelClient(host, port)));  // This method generate multiple TIME_WAIT
            _sentinels.AddLast(Tuple.Create<string, int, RedisSentinelClient>(host, port, new RedisConnectionPool(host, port, 5).GetSentinelClient()));
        }

        public static void StartCheckingSentinels(string[] sentinels, string mastername, int check_sentinels_seconds)
        {   
            _sentinels = new LinkedList<Tuple<string, int, RedisSentinelClient>>();
            foreach (var host in sentinels)
            {
                Global.Log("Add Sentinel for Checking: " + host);
                string[] parts = host.Split(':');
                string hostname = parts[0].Trim();
                int port = Int32.Parse(parts[1]);
                Add(hostname, port);
            }

            var task = Repeat.Interval(
                    TimeSpan.FromMilliseconds(check_sentinels_seconds * 1000),
                    () => CheckSentinels(mastername), cancellationTokenSource.Token);
        }

        public static Tuple<string, int> GetMaster(string mastername)
        {
            try
            {
                Tuple<string, int> master = null;
                int attempts = 0;
                bool retry = true;
                while ((attempts < 3) && (retry))
                {
                    retry = false;
                    for (int i = 0; i < _sentinels.Count; i++)
                    {
                        if (i > 0)
                            Next();

                        using (var sentinel = _sentinels.First.Value.Item3)
                        {                           
                            //Global.Log("Querying sentinel: " + sentinel.Host + ":" + sentinel.Port);
                            sentinel.ReconnectAttempts = 2; 
                            if (!sentinel.Connect(timeout)) continue;

                            master = sentinel.GetMasterAddrByName(mastername);

                            if (master == null)
                                continue;

                            /* Check Master connectivity
                            RedisClient _redisClient = new RedisClient(master.Item1, master.Item2);

                            try
                            {
                                if (!(TryerHelper.Try(delegate {
                                    Global.Log("Checking master: " + master.Item1 + ":" + master.Item2);
                                    return _redisClient.Connect(timeout);                                        
                                })).Retry(2).IgnoreException(typeof(SocketException)).Execute<bool>()) continue;
                                
                            }
                            catch (TryerHelper.TryerException)
                            {
                                Global.Log("Master is not responding, performing manual Failover: " + sentinel.Host + ":" + sentinel.Port);
                                try {
                                    sentinel.Failover(mastername);
                                }
                                catch (Exception ex) {
                                    Global.Log("Manual Failover Failed, Slave possible down, attempting to connect to other sentinel.");
                                    continue; // Continue with the next sentinel.
                                }
                                retry = true;
                                break;
                            }

                            var role = _redisClient.Role();
                            _redisClient.Quit();

                            if (role.RoleName != "master")
                                continue;
                            */

                            if (master == null)
                                continue;

                            foreach (var remoteSentinel in sentinel.Sentinels(mastername))
                                Add(remoteSentinel.Ip, remoteSentinel.Port);

                            return master;
                        }
                    }
                    attempts++;
                }
                return master;
            }
            catch (Exception ex)
            {
                Global.Log("GetMaster exception: " + ex.Message);
                return null;
            }
        }

        public static void CheckSentinels(string mastername)
        {
            try
            {
                var master = GetMaster(mastername);
                if (SentinelCheck != null)
                    SentinelCheck(null, null);
                if (master != null)
                {
                    string newMaster = master.Item1 + ":" + master.Item2;
                    if (newMaster != Global.masterIP)
                    {
                        Global.masterIP = newMaster;
                        Global.Log("Current Master Is: " + Global.masterIP);
                        if (Masterchange != null)
                            Masterchange(null, null);
                        //SocketConnectionPool.InitializeConnectionPool(master.Item1, master.Item2, 1, 3);
                    } 

                }
                else
                {
                    Global.Log("Could not determine the master Redis server. ");
                }
            }
            catch (Exception ex)
            {
                Global.Log("Read exception: " + ex.ToString());
            }            
        }

        static void Next()
        {
            var first = _sentinels.First;
            _sentinels.RemoveFirst();
            _sentinels.AddLast(first.Value);
        }

        internal static void StopCheckingSentinels()
        {
            Global.Log("Stop Sentinel Checking. ");
            cancellationTokenSource.Cancel();
        }
    }
}
