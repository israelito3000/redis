﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisMonitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Global.Log("Starting...");
            StartUp();
            Global.Log("System running; press any key to stop");
            Console.ReadKey(true);
            ShutDown();
            Global.Log("System stopped");
        }

        private static void ShutDown()
        {
            RedisSentinelChecker.StopCheckingSentinels();
        }

        public static void StartUp()
        {

            string[] sentinels = ConfigurationManager.AppSettings["sentinels"].Split(',');
            int check_sentinels_seconds = int.Parse(ConfigurationManager.AppSettings["check_sentinels_seconds"]);
            RedisSentinelChecker.StartCheckingSentinels(sentinels, "mymaster", check_sentinels_seconds);
        }

    }
}
