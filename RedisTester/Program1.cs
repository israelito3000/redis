﻿using CSRedis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RedisTester
{
    class Program1
    {


        static void Main(string[] args)
        {
            //string c = Read("172.19.99.235:8098");
            //if (c == null) Console.WriteLine("Balancer: Customer not found.");

            //shared = ConnectionMultiplexer.Connect("172.19.99.232:8098,172.19.99.233:8098");

            if (args.Length == 0)
            {
                args = new string[1] { "172.21.176.85:8099" };
            }

            while (true)
            {

                string c = "";

                c = Read(args);
                if (c == null)
                {
                    Console.WriteLine("MultiRead: Customer not found, Attempt #1");
                    c = Read(args);
                    if (c == null)
                    {
                        Console.WriteLine("MultiRead: Customer not found, Attempt #2");
                    }
                }

                if (c == null)
                {
                    Write(args);
                    Console.WriteLine("Write: Write to Redis.");
                }               

            }

            Console.WriteLine("Finished.");
            Console.ReadLine();
        }

        public static string RandomStr(int length)
        {
            string range = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Byte[] bytes = new Byte[length];
            char[] chars = new char[length];

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(bytes);

            for (int i = 0; i < length; i++)
                chars[i] = range[bytes[i] % range.Length];

            return new string(chars);
        }

        public static void Write(string[] args)
        {
            try
            {
                Console.WriteLine("Connecting to Sentinel: " + string.Join(",",args[0]));
                using (var sentinel = new RedisSentinelManager(args))
                {                    
                    Customer cus = new Customer();
                    cus.FirstName = RandomStr(10);
                    cus.LastName = RandomStr(10);
                    cus.CustomerId = 1;
                    cus.Created = DateTime.Now;
                    string str = JsonConvert.SerializeObject(cus);

                    sentinel.Connect("mymaster");
                    Console.WriteLine("Master is: " + sentinel.Client.Host + ":" + sentinel.Client.Port);
                    sentinel.Call(x => x.Set("customer", str));
                    sentinel.Call(x => x.Expire("customer", 60)); 
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Write exception: " + ex.Message);
            }
        }


        public static string Read(string[] args)
        {
            try
            {
                Console.WriteLine("Connecting to Sentinel: " + string.Join(",", args[0]));
                using (var sentinel = new RedisSentinelManager(args))
                {
                    sentinel.Connect("mymaster");
                    Console.WriteLine("Master is: " + sentinel.Client.Host + ":" + sentinel.Client.Port);
                    string customer = sentinel.Call(x => x.Get("customer"));
                    if (customer != null)
                    {
                        long seconds = sentinel.Call(x => x.Ttl("customer"));
                        Console.WriteLine("Time to live: " + seconds);
                        if (seconds < 0) sentinel.Call(x => x.Expire("customer", 0)); 
                    }
                    return customer;
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Read exception: " + ex.ToString());
                return null;
            }
        }
    }
}
