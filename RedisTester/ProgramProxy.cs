﻿using CSRedis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace RedisTester
{
    class ProgramProxy
    {


        static void Main(string[] args)
        {
            //string c = Read("172.19.99.235:8098");
            //if (c == null) Console.WriteLine("Balancer: Customer not found.");

            //shared = ConnectionMultiplexer.Connect("172.19.99.232:8098,172.19.99.233:8098");

            Console.ReadLine();

            if (args.Length == 0)
            {
                args = new string[1] { "127.0.0.1:8097" }; // Proxy
                //args = new string[1] { "172.21.176.85:8097" }; // Proxy
                //args = new string[1] { "172.21.176.51:8098" };
                
            }

            while (true)
            {

                string c = "";

                c = Read(args);
                if (c == null)
                    Console.WriteLine("MultiRead: Customer not found");

                if (c == null)
                {
                    Write(args);
                    Console.WriteLine("Write: Write to Redis.");
                }               

            }

            Console.WriteLine("Finished.");
            Console.ReadLine();
        }

        public static string RandomStr(int length)
        {
            string range = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Byte[] bytes = new Byte[length];
            char[] chars = new char[length];

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(bytes);

            for (int i = 0; i < length; i++)
                chars[i] = range[bytes[i] % range.Length];

            return new string(chars);
        }

        public static void Write(string[] args)
        {
            try
            {

                Console.WriteLine("Connecting to Redis: " + args[0]);
                string[] addr = args[0].Split(':');
                using (var client = new RedisClient(addr[0], int.Parse(addr[1])))
                {
                    client.ReconnectAttempts = 2;
                    Customer cus = new Customer();
                    cus.FirstName = RandomStr(10);
                    cus.LastName = RandomStr(10);
                    cus.CustomerId = 1;
                    cus.Created = DateTime.Now;
                    string str = JsonConvert.SerializeObject(cus);

                    client.Connect(500);
                    client.ReceiveTimeout = 500;

                    client.Set("customer", str);
                    client.Expire("customer", 60);
                    client.Quit();                    
                }               

            }
            catch (Exception ex)
            {
                Console.WriteLine("Write exception: " + ex.Message);
            }
        }


        public static string Read(string[] args)
        {
            try
            {
                Console.WriteLine("Connecting to Redis: " + args[0]);
                string[] addr = args[0].Split(':');
                using (var client = new RedisClient(addr[0], int.Parse(addr[1])))
                {
                    client.ReconnectAttempts = 2;                    
                    client.Connect(500);
                    client.ReceiveTimeout = 500;
                    string customer = client.Get("customer");
                    if (customer != null)
                    {
                        long seconds = client.Ttl("customer");
                        Console.WriteLine("Time to live: " + seconds);
                    }
                    client.Quit();
                    return customer;                               
                }               
            }
            catch (Exception ex)
            {
                Console.WriteLine("Read exception: " + ex.ToString());
                return null;
            }
        }
    }
}
