﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RedisTester
{
    class Program
    {

        static ConnectionMultiplexer shared = null;

        static void Main(string[] args)
        {
            //string c = Read("172.19.99.235:8098");
            //if (c == null) Console.WriteLine("Balancer: Customer not found.");

            //shared = ConnectionMultiplexer.Connect("172.19.99.232:8098,172.19.99.233:8098");

            while (true)
            {

                string c = "";

                c = Read("172.19.99.232:8098,172.19.99.233:8098");
                if (c == null) Console.WriteLine("MultiRead: Customer not found.");

                if (c == null)
                {
                    Write("172.19.99.232:8098,172.19.99.233:8098");
                    Console.WriteLine("Write: Write to Redis.");
                }

                c = Read("172.19.99.232:8098,172.19.99.233:8098");
                if (c == null) Console.WriteLine("MultiRead: Customer not found.");

            }

            Console.WriteLine("Finished.");
            Console.ReadLine();
        }

        public static string RandomStr(int length)
        {
            string range = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Byte[] bytes = new Byte[length];
            char[] chars = new char[length];

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(bytes);

            for (int i = 0; i < length; i++)
                chars[i] = range[bytes[i] % range.Length];

            return new string(chars);
        }

        public static void Write(string servers)
        {
            try
            {
                ConfigurationOptions co = new ConfigurationOptions();
                co.AbortOnConnectFail = true;
                co.ConnectRetry = 0;
                //co.ConnectTimeout = 2000;
                string[] enpoints = servers.Split(',');
                foreach (string ep in enpoints)
                    co.EndPoints.Add(ep);
                
                ConnectionMultiplexer redis = (shared !=null ? shared : ConnectionMultiplexer.Connect(co));


                IDatabase db = redis.GetDatabase();
                Customer cus = new Customer();
                cus.FirstName = RandomStr(10);
                cus.LastName = RandomStr(10);
                cus.CustomerId = 1;
                cus.Created = DateTime.Now;
                string str = JsonConvert.SerializeObject(cus);
                db.StringSet("customer", str);
                db.KeyExpire("customer", DateTime.Now.AddMinutes(1));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Write exception: " + ex.Message);
            }
        }


        public static string Read(string servers)
        {
            try
            {
                ConfigurationOptions co = new ConfigurationOptions();
                co.AbortOnConnectFail = false;
                co.ConnectRetry = 0;
                //co.ConnectTimeout = 2000;
                string[] enpoints = servers.Split(',');
                foreach (string ep in enpoints)
                    co.EndPoints.Add(ep);

                ConnectionMultiplexer redis = (shared != null ? shared : ConnectionMultiplexer.Connect(co));

                IDatabase db = redis.GetDatabase();
                string customer = db.StringGet("customer");

                if (customer != null)
                {
                    TimeSpan? ts = db.KeyTimeToLive("customer");
                    if (ts != null)
                        Console.WriteLine("Time to live: " + ts.Value.TotalSeconds);

                }

                return customer;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Read exception: " + ex.ToString());
                return null;
            }
        }
    }
}
